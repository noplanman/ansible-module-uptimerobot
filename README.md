# Ansible module for Uptimerobot

This is an Ansible module to manage your monitors on [Uptimerobot].

WIP! Currently only basic monitor management is available!

## Installation

In the root folder of your Ansible project add this project as a submodule:

`$ git submodule add https://gitlab.com/noplanman/ansible-module-uptimerobot library/uptimerobot`

## Usage

**Important notes**:
- When defining `monitor_id` and `friendly_name`, `monitor_id` takes precedence.
- `friendly_name` is used as a unique identifier for a monitor! So expect unexpected things when multiple monitors have the same `friendly_name`. If multiple monitors are to be used on the same host, best label it in the `friendly_name`, for example `example.com - port` or `example.com - keyword`.
- The `type` cannot be changed for an existing monitor. So to change the type of an existing monitor, it must be deleted and created again.

```yaml
# Create a monitor checking for https port
- uptimerobot_monitor:
    api_key: 12345-1234512345
    type: port
    sub_type: https

# Create a monitor checking for port 1337
- uptimerobot_monitor:
    api_key: 12345-1234512345
    type: port
    sub_type: custom
    port: 1337

# Create a monitor checking for non-existence of keyword "whoop"
- uptimerobot_monitor:
    api_key: 12345-1234512345
    type: keyword
    keyword_type: not_exists
    keyword_value: whoop

# Pause the monitor with an ID of 12345.
- uptimerobot_monitor:
    api_key: 12345-1234512345
    monitor_id: 12345
    state: paused

# Start the monitor with an ID of 12345.
- uptimerobot_monitor:
    api_key: 12345-1234512345
    monitor_id: 12345
    state: started

# Delete the monitor with an friendly_name of "example.com".
- uptimerobot_monitor:
    api_key: 12345-1234512345
    friendly_name: example.com
    state: absent
```

## Possible values

This module maps the [Uptimerobot API] quite closely.

## Todo

- Implement a `force` parameter that automatically deletes and recreates a monitor when its `type` is changed.
- Bulk actions using `monitor_ids` list.
- Add missing parameters for `uptimerobot_monitor` task.
- Add support for Public Status Pages as `uptimerobot_public_status_page` task.
- Add support for Alert Contacts as `uptimerobot_alert_contact` task.
- Add support for Maintenance Windows as `uptimerobot_maintenance_window` task.

[Uptimerobot]: https://uptimerobot.com
[Uptimerobot API]: https://uptimerobot.com/api
