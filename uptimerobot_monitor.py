#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright: Armando Lüscher (@noplanman)
# Based on official Ansible uptimerobot task: https://github.com/ansible/ansible/blob/devel/lib/ansible/modules/monitoring/uptimerobot.py
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type

import json

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url


class UptimerobotMonitor:
    SUPPORTS_CHECK_MODE = False
    API_BASE = 'https://api.uptimerobot.com/v2/'
    API_FORMAT = 'json'

    MONITOR_TYPES = dict(https='1', keyword='2', ping='3', port='4')
    MONITOR_SUB_TYPES = dict(http='1', https='2', ftp='3', smtp='4', pop3='5', imap='6', custom='99')
    KEYWORD_TYPES = dict(exists='1', not_exists='2')
    MONITOR_STATUS = dict(paused='0', not_checked_yet='1', up='2', seems_down='8', down='9')

    params = None
    monitors = None

    module = AnsibleModule(
        argument_spec=dict(
            api_key=dict(required=True, no_log=True, type='str'),
            state=dict(default='present', choices=['fetch', 'present', 'absent', 'started', 'paused', 'reset']),
            monitor_id=dict(type='int'),
            # monitor_ids=dict(type='list'),
            friendly_name=dict(type='str'),
            url=dict(type='str'),
            type=dict(choices=MONITOR_TYPES.keys(), type='str'),
            sub_type=dict(choices=MONITOR_SUB_TYPES.keys(), type='str'),
            port=dict(type='int'),
            keyword_type=dict(choices=KEYWORD_TYPES.keys(), type='str'),
            keyword_value=dict(type='str'),
            interval=dict(type='int'),
            http_username=dict(type='str'),
            http_password=dict(no_log=True, type='str'),
            alert_contacts=dict(type='str'),
            mwindows=dict(type='str'),
            custom_http_headers=dict(type='str'),
            custom_http_statuses=dict(type='str'),
            ignore_ssl_errors=dict(type='bool'),
        ),
        required_together=[['keyword_type', 'keyword_value']],
        required_one_of=[['state', 'friendly_name', 'monitor_id']],  # , 'monitor_ids'
        required_if=[
            ['state', 'present', ['type', 'friendly_name', 'url']],
            ['type', 'port', ['sub_type']],
            ['sub_type', 'custom', ['port']],
            ['type', 'keyword', ['keyword_type', 'keyword_value']],
        ],
        supports_check_mode=SUPPORTS_CHECK_MODE
    )

    params_base = dict(
        api_key=module.params['api_key'],
        format=API_FORMAT,
    )

    def api(self, action, params=None):
        if params is None:
            params = dict()

        params_all = self.params_base.copy()
        params_all.update(params)

        req, info = fetch_url(
            self.module,
            self.API_BASE + action,
            method='POST',
            headers={'Content-Type': 'application/json'},
            data=self.module.jsonify(params_all),
        )

        result = req.read()
        result_json = json.loads(result.decode('utf-8'))
        req.close()

        return result_json

    def fetchMonitors(self, refresh=False):
        result = None
        if self.monitors is None or refresh:
            result = self.api('getMonitors', dict(alert_contacts='1', custom_http_headers='1', custom_http_statuses='1'))
            self.monitors = result.get('monitors', None)
        return result

    def startMonitor(self, params):
        params['status'] = '1'
        return self.editMonitor(params, 'monitor started')

    def pauseMonitor(self, params):
        params['status'] = '0'
        return self.editMonitor(params, 'monitor paused')

    def editMonitor(self, params, message=''):
        result = None
        changed = False
        failed = True

        monitor = self.getMonitor(params)
        if monitor:
            params['id'] = monitor['id']
            if params.get('monitor_id'):
                del params['monitor_id']

            result = self.api('editMonitor', params)
            if result['stat'] == 'ok':
                # changed = not self.paramsMatch(self.getMonitor(monitor_id=monitor['id'], refresh=True), monitor)
                changed = monitor != self.getMonitor(monitor_id=monitor['id'], refresh=True)
                failed = False
            else:
                message = result['error']['message']
        else:
            message = 'monitor not found'

        return result, changed, failed, message

    def newMonitor(self, params):
        if params.get('monitor_id'):
            params['id'] = params['monitor_id']
            del params['monitor_id']

        changed = False
        failed = True

        result = self.api('newMonitor', params)
        if result['stat'] == 'ok':
            changed = True
            failed = False
            message = 'monitor %s created' % result['monitor']['id']
        else:
            message = result['error']['message']

        return result, changed, failed, message

    def resetMonitor(self, params):
        result = None
        changed = False
        failed = True
        message = 'monitor not found'

        monitor = self.getMonitor(params)
        if monitor:
            result = self.api('resetMonitor', dict(id=monitor['id']))
            if result['stat'] == 'ok':
                changed = True
                failed = False
                message = 'monitor %s reset' % monitor['id']
            else:
                message = result['error']['message']

        return result, changed, failed, message

    def deleteMonitor(self, params):
        result = None
        changed = False
        failed = False
        message = 'monitor not found'

        monitor = self.getMonitor(params)
        if monitor:
            result = self.api('deleteMonitor', dict(id=monitor['id']))
            if result['stat'] == 'ok':
                changed = True
                message = 'monitor %s deleted' % monitor['id']
            else:
                failed = True
                message = result['error']['message']
        # else:
        #     failed = True

        return result, changed, failed, message

    def getMonitor(self, params=None, monitor_id=None, friendly_name=None, refresh=False):
        self.fetchMonitors(refresh)

        if not params:
            params = self.params

        if not monitor_id:
            monitor_id = params.get('monitor_id', None)
        if not friendly_name:
            friendly_name = params.get('friendly_name', None)

        for monitor in self.monitors:
            if monitor['id'] == monitor_id or monitor['friendly_name'] == friendly_name:
                return monitor

    def getKeyFromValue(self, val, dic):
        for k, v in dic.items():
            if v == val:
                return k

    # def derefIds(self, monitor):
    #     monitor['type'] = self.getKeyFromValue(str(monitor['type']), self.MONITOR_TYPES)
    #     monitor['sub_type'] = self.getKeyFromValue(str(monitor['sub_type']), self.MONITOR_SUB_TYPES)
    #     monitor['status'] = self.getKeyFromValue(str(monitor['status']), self.MONITOR_STATUS)
    #     monitor['keyword_type'] = self.getKeyFromValue(str(monitor['keyword_type']), self.KEYWORD_TYPES)
    #     return monitor

    def loadParams(self):
        self.params = dict()

        if self.module.params.get('monitor_id', None):
            self.params['monitor_id'] = int(self.module.params['monitor_id'])
        # if self.module.params.get('monitor_ids', None):
        #     self.params['monitor_ids'] = self.module.params['monitor_ids'].astype('int')
        if self.module.params.get('friendly_name', None):
            self.params['friendly_name'] = self.module.params['friendly_name']
        if self.module.params.get('url', None):
            self.params['url'] = self.module.params['url']

        if self.module.params.get('type', None):
            self.params['type'] = str(self.MONITOR_TYPES[self.module.params['type']])
        if self.module.params.get('sub_type', None):
            self.params['sub_type'] = str(self.MONITOR_SUB_TYPES[self.module.params['sub_type']])
        if self.module.params.get('port', None):
            self.params['port'] = str(self.module.params['port'])

        if self.module.params.get('keyword_type', None):
            self.params['keyword_type'] = str(self.KEYWORD_TYPES[self.module.params['keyword_type']])
        if self.module.params.get('keyword_value', None):
            self.params['keyword_value'] = str(self.module.params['keyword_value'])

        if self.module.params.get('interval', None):
            self.params['interval'] = str(self.module.params['interval'])

        if self.module.params.get('http_username', None):
            self.params['http_username'] = str(self.module.params['http_username'])
        if self.module.params.get('http_password', None):
            self.params['http_password'] = str(self.module.params['http_password'])

        if self.module.params.get('alert_contacts', None):
            self.params['alert_contacts'] = str(self.module.params['alert_contacts'])
        if self.module.params.get('mwindows', None):
            self.params['mwindows'] = str(self.module.params['mwindows'])

        if self.module.params.get('custom_http_headers', None):
            self.params['custom_http_headers'] = str(self.module.params['custom_http_headers'])
        if self.module.params.get('custom_http_statuses', None):
            self.params['custom_http_statuses'] = str(self.module.params['custom_http_statuses'])

        if self.module.params.get('ignore_ssl_errors', None):
            self.params['ignore_ssl_errors'] = str(self.module.params['ignore_ssl_errors'])

        # if self.module.params.get('force', None):
        #     self.params['force'] = bool(self.module.params['force'])

    def paramsMatch(self, params_haystack, params_needle=None):
        if not params_needle:
            params_needle = self.params

        for key, param in params_needle.items():
            try:
                if str(param) != str(params_haystack[key]):
                    return False
            except KeyError:
                continue

        return True

    def __init__(self):
        result = self.fetchMonitors()
        if result['stat'] == 'fail':
            self.module.fail_json(msg='failed to fetch monitors', result=result['error']['message'])

        self.loadParams()

        result = None
        changed = False
        failed = False
        message = ''

        state = self.module.params.get('state', None)

        if state == 'fetch':
            result = self.monitors
            message = 'showing all monitors'
        elif state == 'started':
            result, changed, failed, message = self.startMonitor(self.params)
        elif state == 'paused':
            result, changed, failed, message = self.pauseMonitor(self.params)
        elif state == 'present':
            monitor = self.getMonitor(self.params)

            if not monitor:
                result, changed, failed, message = self.newMonitor(self.params)
            elif not self.paramsMatch(monitor):
                result, changed, failed, message = self.editMonitor(self.params, 'monitor %s updated' % monitor['id'])
            else:
                self.module.exit_json(msg="monitor %s present" % monitor['id'], result=monitor)
                # More user-readable output maybe?
                # self.module.exit_json(msg="monitor %s present" % monitor['id'], result=self.derefIds(monitor))

        elif state == 'absent':
            result, changed, failed, message = self.deleteMonitor(self.params)
        elif state == 'reset':
            result, changed, failed, message = self.resetMonitor(self.params)

        self.module.exit_json(msg=message, result=result, changed=changed, failed=failed)


if __name__ == '__main__':
    UptimerobotMonitor()
